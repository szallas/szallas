<?php

declare(strict_types=1);

namespace Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

/**
 * @group feature
 * @group api
 * @group company
 */
class CompanyTest extends TestCase
{
    use DatabaseMigrations;

    private const BASE_ENDPOINT = '/api/companies/';

    public function testCompany_index(): void
    {
        /** @var Company[]|Collection $companies */
        $companies = Company::factory()->count(3)->create();
        $companyIds = $companies->map(fn(Company $company) => $company->id)->toArray();
        $response = $this->get(self::BASE_ENDPOINT)->json('data');
        $this->assertCount($companies->count(), $response);
        foreach ($response as $responseCompany) {
            $this->assertContains($responseCompany['id'], $companyIds);
        }
    }

    public function testCompany_create(): void
    {
        /** @var Company $company */
        $company = Company::factory()->make();
        $response = $this->post(self::BASE_ENDPOINT, $company->toArray())->json('data');
        $this->assertNotNull($response['id']);
        $this->assertEquals($company->name, $response['name']);
        $this->assertEquals($company->longitude, $response['longitude']);
    }

    public function testCompany_read(): void
    {
        /** @var Company $company */
        $company = Company::factory()->count(3)->create()->random();
        $response = $this->get(self::BASE_ENDPOINT . $company->id)->json('data');
        $this->assertEquals($company->id, $response['id']);
    }

    public function testCompany_update(): void
    {
        /** @var Company $company */
        $company = Company::factory()->create();
        $input = $company->toArray();
        $input['owner_name'] = 'Teszt Elek';
        $response = $this->put(self::BASE_ENDPOINT . $company->id, $input)->json('data');
        $this->assertEquals($company->id, $response['id']);
        $this->assertEquals($company->name, $response['name']);
        $this->assertEquals($input['owner_name'], $response['owner_name']);
        $this->assertEquals('Teszt Elek', $response['owner_name']);
    }

    public function testCompany_delete(): void
    {
        /** @var Company $company */
        $company = Company::factory()->count(3)->create()->random();
        $response = $this->delete(self::BASE_ENDPOINT . $company->id);
        $response->assertStatus(Response::HTTP_OK);
        $this->assertEquals(2, Company::all()->count());
        $this->assertNull(Company::find($company->id));
    }
}
