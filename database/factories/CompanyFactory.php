<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'registration_number' => fake()->regexify('[0-9]{6}-[0-9]{4}'),
            'founded_at' => Carbon::yesterday(),
            'country' => fake()->country,
            'zipcode' => fake()->postcode,
            'city' => fake()->city,
            'address' => fake()->address,
            'latitude' => fake()->latitude,
            'longitude' => fake()->longitude,
            'owner_name' => fake()->name,
            'employee_count' => fake()->numberBetween(1, 30),
            'activity' => fake()->jobTitle,
            'is_active' => fake()->boolean,
            'email' => fake()->email,
            'password' => fake()->password,
        ];
    }
}
